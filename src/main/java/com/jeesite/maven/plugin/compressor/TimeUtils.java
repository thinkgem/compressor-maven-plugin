/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.maven.plugin.compressor;

/**
 * 时间计算工具类
 * @author ThinkGem
 * @version 2015-6-20
 */
public class TimeUtils {
	
	/**
	 * 将毫秒数转换为：xx天，xx时，xx分，xx秒
	 */
	public static String formatDateAgo(long millisecond) {
		long ms = millisecond;
		int ss = 1000;
		int mi = ss * 60;
		int hh = mi * 60;
		int dd = hh * 24;
		long day = ms / dd;
		long hour = (ms - day * dd) / hh;
		long minute = (ms - day * dd - hour * hh) / mi;
		long second = (ms - day * dd - hour * hh - minute * mi) / ss;
		StringBuilder sb = new StringBuilder();
		if (ms >= 0 && ms < 1000) {
			sb.append(ms).append("毫秒");
		} else {
			if (day > 0) {
				sb.append(day).append("天");
			}
			if (hour > 0) {
				sb.append(hour).append("时");
			}
			if (minute > 0) {
				sb.append(minute).append("分");
			}
			if (second > 0) {
				sb.append(second).append("秒");
			}
		}
		return sb.toString();
	}

}