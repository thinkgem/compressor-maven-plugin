package com.jeesite.maven.plugin.compressor;

import java.io.File;

public class SourceFile {

    private File srcRoot;
    private File destRoot;
    private boolean destAsSource;
    private String rpath;
    private String extension;

    public SourceFile(File srcRoot, File destRoot, String name, boolean destAsSource) throws Exception {
        this.srcRoot = srcRoot;
        this.destRoot = destRoot;
        this.destAsSource = destAsSource;
        this.rpath = name;
        int sep = this.rpath.lastIndexOf('.');
        if (sep>0) {
            extension = this.rpath.substring(sep);
            this.rpath = this.rpath.substring(0, sep);
        } else {
            extension = "";
        }
    }

    public File toFile() {
        String frpath = rpath + extension;
        if (destAsSource) {
            File defaultDest = new File(destRoot, frpath);
            if (defaultDest.exists() && defaultDest.canRead()) {
                return defaultDest;
            }
        }
        return new File(srcRoot, frpath);
    }

    public File toDestFile(String suffix) {
        return new File(destRoot, rpath + suffix + extension);
    }

    public String getExtension() {
        return extension;
    }

	public File getSrcRoot() {
		return srcRoot;
	}

	public File getDestRoot() {
		return destRoot;
	}
}
